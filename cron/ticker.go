package cron

import (
	"github.com/gorhill/cronexpr"
	"time"
)

type CronTicker struct {
	C      chan time.Time
	ticker *time.Ticker
	exp    *cronexpr.Expression
}

func NewCronTicker(line string) (*CronTicker, error) {
	exp, err := cronexpr.Parse(line)
	if err != nil {
		return nil, err
	}
	c := make(chan time.Time, 1)
	t := &CronTicker{
		C:      c,
		ticker: time.NewTicker(1 * time.Second),
		exp:    exp,
	}
	go t.startTicker()
	return t, nil
}

func (t *CronTicker) Stop() {
	t.ticker.Stop()
}

func (t *CronTicker) startTicker() {
	now := time.Now()
	nxt := t.exp.Next(now)
	//println(nxt.Format(time.RFC3339))
	for range t.ticker.C {
		now = time.Now()
		if now.Unix() >= nxt.Unix() {
			nxt = t.exp.Next(now)
			//println(nxt.Format(time.RFC3339))
			t.C <- now
		}
	}
}
