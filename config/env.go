package config

import (
	log "github.com/Sirupsen/logrus"
	"os"
	"strconv"
	"strings"
	"time"
)

func GetEnvString(name, def string) (value string) {
	if value = os.Getenv(name); value == "" {
		value = def
	}
	return
}

func GetEnvBool(name string, def bool) bool {
	v := os.Getenv(name)
	if v != "" {
		v = strings.ToLower(v)
		return v == "1" || v == "yes" || v == "true" || v == "y"
	}
	return def
}

func GetEnvInt(name string, def int) int {
	v := GetEnvInt64(name, int64(def))
	return int(v)
}

func GetEnvInt64(name string, def int64) int64 {
	v := os.Getenv(name)
	if v != "" {
		i64, err := strconv.ParseInt(v, 10, 0)
		if err != nil {
			log.Warnf("Parse int64 value error, env var %s: %s", name, err.Error())
			return def
		}
		return i64
	}
	return def
}

func GetEnvDuration(name string, def time.Duration) time.Duration {
	v := os.Getenv(name)
	if v != "" {
		d, err := time.ParseDuration(v)
		if err != nil {
			log.Warnf("Parse duration value error, env var %s: %s", name, err.Error())
			return def
		}
		return d
	}
	return def
}
