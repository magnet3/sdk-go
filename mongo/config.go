package mongo

import (
	"gitlab.com/magnet3/sdk-go/config"
	"time"
)

type MongoConfig struct {
	Host    string
	Port    int
	Timeout time.Duration
	DB      string
}

func ReadMongoConfig() MongoConfig {
	return MongoConfig{
		Host:    config.GetEnvString("MONGO_HOST", "127.0.0.1"),
		Port:    config.GetEnvInt("MONGO_PORT", 27017),
		Timeout: config.GetEnvDuration("MONGO_TIMEOUT", 1*time.Second),
		DB:      config.GetEnvString("MONGO_DB", "magnet"),
	}
}
