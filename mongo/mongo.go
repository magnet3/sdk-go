package mongo

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"gopkg.in/mgo.v2"
	"time"
)

type Mongo struct {
	cfg     MongoConfig
	session *mgo.Session
	DB      *mgo.Database
}

func Connect(cfg MongoConfig) (*Mongo, error) {
	m := &Mongo{
		cfg: cfg,
	}
	err := m.connect()
	if err == nil {
		go m.watch(1 * time.Second)
	}
	return m, err
}

func (m *Mongo) Disconnect() {
	m.session.Close()
}

func (m *Mongo) connect() error {
	if m.session != nil {
		return nil
	}
	url := fmt.Sprintf("mongodb://%s:%d/", m.cfg.Host, m.cfg.Port)
	session, err := mgo.DialWithTimeout(url, m.cfg.Timeout)
	if err != nil {
		return err
	}
	m.session = session
	m.DB = session.DB(m.cfg.DB)
	return nil
}

func (m *Mongo) watch(timeout time.Duration) {
	t := time.NewTicker(timeout)
	for range t.C {
		err := m.session.Ping()
		if err == nil {
			log.Debug("Mongo ping success")
			continue
		}
		log.Errorf("Mongo ping error: %s", err.Error())
		m.session = nil
		err = m.connect()
		if err == nil {
			log.Info("Mongo success reconnect")
		} else {
			log.Errorf("Mongo reconnect error error: %s", err.Error())
		}
	}
}
