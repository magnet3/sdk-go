SDK для микросервисов на языке Go
=================================

> ![Bear!](logo.png)
>
> Потому, что "Go beer!"

-------------------------

### Примеры

* `examples/helloworld.go` - Hello World для JSON API, базовое использование и минимальный функционал
* `examples/mysql.go` - Пример использования MySQL
* `examples/mongo.go` - Пример использования MongoDB
* `examples/sphinx.go` - Пример использования Sphinx Search

### GitLab и govendor

Как известно GitLab для приватных проектов требует авторизацию. Утилита `govendor` ничего не знает о авторизации. Придется ей помочь (и себе то же):

* Идем на страницу где [дают токены](https://gitlab.com/profile/personal_access_tokens)
* Вызываем `$ git config --global url."https://<account>:<token>@gitlab.com/".insteadOf "https://gitlab.com/"`

Теперь можно вызывать `govendor sync` и не мучаемся с авторизацией для каждого репозитория в отдельности.
