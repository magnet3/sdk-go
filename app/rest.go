package app

import (
	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
	"gitlab.com/magnet3/sdk-go/api/middleware"
	logger "gitlab.com/magnet3/sdk-go/log"
	"gopkg.in/tylerb/graceful.v1"
	"net/http"
	"time"
)

func InitRestApi(debug bool) *rest.Api {
	logger.StdoutLogging(debug)
	app := rest.NewApi()
	if debug {
		log.Info("Run application in debug mode")
		app.Use(middleware.DevStackkDefault()...)
	} else {
		log.Info("Run application")
		app.Use(middleware.ProdStackDefault()...)
	}
	return app
}

func RunRestApi(addr string, api *rest.Api, router rest.App) {
	api.SetApp(router)
	server := &graceful.Server{
		Timeout:      5 * time.Second,
		TCPKeepAlive: 4 * time.Second,
		Server: &http.Server{
			Addr:    addr,
			Handler: api.MakeHandler(),
		},
	}
	log.Infof("Bind on %s", addr)
	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	} else {
		log.Info("Shutdown")
	}
}

func RestDefault(debug bool, addr string, router rest.App) {
	api := InitRestApi(debug)
	RunRestApi(addr, api, router)
}
