package main

import (
	"gitlab.com/magnet3/sdk-go/api"
	"gitlab.com/magnet3/sdk-go/app"
	"gitlab.com/magnet3/sdk-go/config"
	log "github.com/Sirupsen/logrus"
	logging "gitlab.com/magnet3/sdk-go/log"
)


var addr string
var debug bool

type SimpleController struct {}

func init() {
	addr = config.GetEnvString("BIND_ADDR", "0.0.0.0:8080")
	debug = config.GetEnvBool("DEBUG", false)
	logging.StdoutLogging(debug)
}

func main() {
	router, err := api.MakeControllerRouter(
		api.AboutController("./about.json"),
	)
	if err != nil {
		log.Fatal(err.Error())
	}
	app.RestDefault(debug, addr, router)
}
