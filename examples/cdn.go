package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
	"gitlab.com/magnet3/sdk-go/api"
	"gitlab.com/magnet3/sdk-go/app"
	"gitlab.com/magnet3/sdk-go/config"
	"gitlab.com/magnet3/sdk-go/discovery"
	"gitlab.com/magnet3/sdk-go/discovery/sibset_cdn"
	logging "gitlab.com/magnet3/sdk-go/log"
	"io/ioutil"
)

var addr string
var debug bool

type CdnExampleController struct {
	cdn *sibset_cdn.Binding
}

func NewCdnController(b *sibset_cdn.Binding) *CdnExampleController {
	return &CdnExampleController{
		cdn: b,
	}
}

func (c *CdnExampleController) Routes() []*rest.Route {
	return []*rest.Route{
		rest.Post("/posters", c.handlePostImage),
	}
}

func (c *CdnExampleController) handlePostImage(w rest.ResponseWriter, req *rest.Request) {
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		api.BadRequest(w, err.Error())
		return
	}
	url, err := c.cdn.GetCDN().UploadContent(b, "poster.png")
	if err != nil {
		api.InternalError(w, err.Error())
		return
	}
	api.SuccessResult(w, url)
}

func init() {
	addr = config.GetEnvString("BIND_ADDR", "0.0.0.0:8080")
	debug = config.GetEnvBool("DEBUG", false)
	logging.StdoutLogging(debug)
}

func main() {
	c, err := discovery.NewConsul("10.0.3.2", 8500)
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := sibset_cdn.BindDefault(c)
	if err != nil {
		log.Fatal(err.Error())
	}
	router, err := api.MakeControllerRouter(
		NewCdnController(b),
		api.AboutController("./about.json"),
	)
	if err != nil {
		log.Fatal(err.Error())
	}
	app.RestDefault(debug, addr, router)
}
