package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
	"gitlab.com/magnet3/sdk-go/api"
	"gitlab.com/magnet3/sdk-go/app"
	"gitlab.com/magnet3/sdk-go/config"
	"gitlab.com/magnet3/sdk-go/discovery"
	"gitlab.com/magnet3/sdk-go/discovery/mongo"
	logging "gitlab.com/magnet3/sdk-go/log"
	"gitlab.com/magnet3/sdk-go/magnet"
	"gopkg.in/mgo.v2/bson"
)

var addr string
var debug bool

type MongoExampleController struct {
	mongo *mongo.Binding
}

func NewMongoController(m *mongo.Binding) *MongoExampleController {
	return &MongoExampleController{
		mongo: m,
	}
}

func (c *MongoExampleController) Routes() []*rest.Route {
	return []*rest.Route{
		rest.Get("/media", c.handleGetMongo),
	}
}

func (c *MongoExampleController) handleGetMongo(w rest.ResponseWriter, req *rest.Request) {
	media := c.mongo.CollectionDefaultDatabase(mongo.MediaCollection)
	m := magnet.Media{}
	err := media.Find(bson.M{"type": "movie"}).One(&m)
	if err != nil {
		api.InternalError(w, err.Error())
		return
	}
	api.SuccessResult(w, m)
}

func init() {
	addr = config.GetEnvString("BIND_ADDR", "0.0.0.0:8080")
	debug = config.GetEnvBool("DEBUG", false)
	logging.StdoutLogging(debug)
}

func main() {
	c, err := discovery.NewConsul("10.0.3.2", 8500)
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := mongo.Bind(c, mongo.Service)
	if err != nil {
		log.Fatal(err.Error())
	}
	router, err := api.MakeControllerRouter(
		NewMongoController(b),
		api.AboutController("./about.json"),
	)
	if err != nil {
		log.Fatal(err.Error())
	}
	app.RestDefault(debug, addr, router)
}
