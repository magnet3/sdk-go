package main

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
	"gitlab.com/magnet3/sdk-go/api"
	"gitlab.com/magnet3/sdk-go/app"
	"gitlab.com/magnet3/sdk-go/config"
	"gitlab.com/magnet3/sdk-go/discovery"
	"gitlab.com/magnet3/sdk-go/discovery/sphinx"
	logging "gitlab.com/magnet3/sdk-go/log"
)

var addr string
var debug bool

type SphinxExampleController struct {
	sphinx *sphinx.Binding
}

func NewSphinxController(s *sphinx.Binding) *SphinxExampleController {
	return &SphinxExampleController{
		sphinx: s,
	}
}

func (c *SphinxExampleController) Routes() []*rest.Route {
	return []*rest.Route{
		rest.Get("/search", c.handleGetExample),
	}
}

func (c *SphinxExampleController) handleGetExample(w rest.ResponseWriter, req *rest.Request) {
	q := req.Request.URL.Query().Get("q")
	query := fmt.Sprintf("^%s$ | ^%s* | %s", q, q, q)
	log.Debugf("Query: %s", query)
	r, err := c.sphinx.Query(query, sphinx.MagnetSpace)
	if err != nil {
		api.InternalError(w, fmt.Sprintf("Sphinx error: %s", err.Error()))
		return
	}
	api.SuccessItems(w, len(r), r)
}

func init() {
	addr = config.GetEnvString("BIND_ADDR", "0.0.0.0:8080")
	debug = config.GetEnvBool("DEBUG", false)
	logging.StdoutLogging(debug)
}

func main() {
	c, err := discovery.NewConsul("10.0.3.2", 8500)
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := sphinx.Bind(c, sphinx.Service)
	if err != nil {
		log.Fatal(err.Error())
	}
	router, err := api.MakeControllerRouter(
		NewSphinxController(b),
		api.AboutController("./about.json"),
	)
	if err != nil {
		log.Fatal(err.Error())
	}
	app.RestDefault(debug, addr, router)
}
