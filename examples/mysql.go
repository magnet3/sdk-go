package main

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
	"gitlab.com/magnet3/sdk-go/api"
	"gitlab.com/magnet3/sdk-go/app"
	"gitlab.com/magnet3/sdk-go/config"
	"gitlab.com/magnet3/sdk-go/discovery"
	"gitlab.com/magnet3/sdk-go/discovery/mysql"
	logging "gitlab.com/magnet3/sdk-go/log"
)

var addr string
var debug bool

type MysqlExampleController struct {
	mysql *mysql.Binding
}

func NewMysqlController(m *mysql.Binding) *MysqlExampleController {
	return &MysqlExampleController{
		mysql: m,
	}
}

func (c *MysqlExampleController) Routes() []*rest.Route {
	return []*rest.Route{
		rest.Get("/history", c.handleGetExample),
	}
}

func (c *MysqlExampleController) handleGetExample(w rest.ResponseWriter, req *rest.Request) {
	db := c.mysql.DefaultDatabase()
	rows, err := db.Query("SELECT h.device, h.media_alias FROM history h ORDER BY h.viewed_at DESC LIMIT ?, ?", 0, 20)
	if err != nil {
		api.InternalError(w, fmt.Sprintf("Query error: %s", err))
		return
	}
	defer rows.Close()
	m := map[string]string{}
	for rows.Next() {
		var device, alias string
		err = rows.Scan(&device, &alias)
		if err != nil {
			api.InternalError(w, fmt.Sprintf("Result scan error: %s", err))
			return
		}
		m[device] = alias
	}
	api.SuccessResult(w, m)
}

func init() {
	addr = config.GetEnvString("BIND_ADDR", "0.0.0.0:8080")
	debug = config.GetEnvBool("DEBUG", false)
	logging.StdoutLogging(debug)
}

func main() {
	c, err := discovery.NewConsul("10.0.3.2", 8500)
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := mysql.Bind(c, mysql.Service)
	if err != nil {
		log.Fatal(err.Error())
	}
	router, err := api.MakeControllerRouter(
		NewMysqlController(b),
		api.AboutController("./about.json"),
	)
	if err != nil {
		log.Fatal(err.Error())
	}
	app.RestDefault(debug, addr, router)
}
