package discovery

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	consul "github.com/hashicorp/consul/api"
	"strconv"
	"time"
)

type Consul struct {
	consul   *consul.Client
	bindings []*ServicesBinding
}

func NewConsul(host string, port int) (*Consul, error) {
	addr := fmt.Sprintf("%s:%d", host, port)
	cnf := &consul.Config{
		Address: addr,
	}
	c, err := consul.NewClient(cnf)
	if err != nil {
		return nil, err
	}
	lead, err := c.Status().Leader()
	if err != nil {
		log.Warn("No Consul connect")
		return nil, err
	} else {
		log.Infof("Consul connected ok, now leader is %s", lead)
	}
	return &Consul{
		consul: c,
	}, nil
}

func (c *Consul) GetStringParam(p string, def string) (r string) {
	defer func() {
		if err := recover(); err != nil {
			r = def
		}
	}()
	v, _, err := c.consul.KV().Get(p, nil)
	if err != nil {
		return def
	}
	r = string(v.Value)
	return r
}

func (c *Consul) GetIntParam(p string, def int) int {
	v := c.GetStringParam(p, "")
	if v == "" {
		return def
	}
	s := string(v)
	r, err := strconv.Atoi(s)
	if err != nil {
		return def
	}
	return r
}

func (c *Consul) GetUintParam(p string, def uint) uint {
	return uint(c.GetIntParam(p, int(def)))
}

func (c *Consul) GetDurationParam(p string, q time.Duration, def time.Duration) time.Duration {
	d := uint(def) / uint(q)
	return time.Duration(c.GetUintParam(p, d)) * q
}

func (c *Consul) DiscoverServices(service string) ([]*consul.CatalogService, error) {
	s, _, err := c.consul.Catalog().Service(service, "", nil)
	if err != nil {
		return nil, err
	}
	if len(s) == 0 {
		return nil, fmt.Errorf("No available service %s", service)
	}
	return s, nil
}

func (c *Consul) DiscoverFirstService(service string) (*consul.CatalogService, error) {
	s, _, err := c.consul.Catalog().Service(service, "", nil)
	if err != nil {
		return nil, err
	}
	for _, r := range s {
		return r, nil
	}
	return nil, fmt.Errorf("No available service %s", service)
}

func (c *Consul) DiscoverServicesWithTag(service string, tag string) ([]*consul.CatalogService, error) {
	s, _, err := c.consul.Catalog().Service(service, tag, nil)
	if err != nil {
		return nil, err
	}
	if len(s) == 0 {
		return nil, fmt.Errorf("No available %s service with tag %s", service, tag)
	}
	return s, nil
}

func (c *Consul) Stop() {
	for _, b := range c.bindings {
		b.unbind()
	}
}
