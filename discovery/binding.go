package discovery

import (
	log "github.com/Sirupsen/logrus"
	"github.com/hashicorp/consul/api"
	"time"
)

var DefaultRefreshTimeout = 10 * time.Second

type ServicesBinding struct {
	consul   *Consul
	id       string
	tag      string
	refresh  time.Duration
	services []*api.CatalogService
	idx      int
	stopping bool
}

func (c *Consul) NewServiceBinding(id string) (*ServicesBinding, error) {
	return c.NewServiceWitTagBinding(id, "", DefaultRefreshTimeout)
}

func (c *Consul) NewServiceWitTagBinding(id string, tag string, refresh time.Duration) (*ServicesBinding, error) {
	b := &ServicesBinding{
		consul:  c,
		id:      id,
		tag:     tag,
		refresh: refresh,
	}
	if err := b.Refresh(); err != nil {
		return nil, err
	}
	c.bindings = append(c.bindings, b)
	b.bind()
	return b, nil
}

func (b *ServicesBinding) Refresh() error {
	s, err := b.consul.DiscoverServicesWithTag(b.id, b.tag)
	if err != nil {
		return err
	}
	b.services = s
	return nil
}

func (b *ServicesBinding) GetAllServices() []api.CatalogService {
	r := make([]api.CatalogService, 0)
	for _, s := range b.services {
		r = append(r, *s)
	}
	return r
}

func (b *ServicesBinding) GetRoundRobinService() api.CatalogService {
	if b.idx == len(b.services) {
		b.idx = 0
	}
	s := b.services[b.idx]
	b.idx = b.idx + 1
	return *s
}

func (b *ServicesBinding) bind() {
	go func() {
		for range time.Tick(b.refresh) {
			log.Debugf("Refres binding %s services", b.id)
			if b.stopping {
				log.Warnf("Stop binding %s services", b.id)
				return
			}
			err := b.Refresh()
			if err != nil || len(b.services) == 0 {
				log.Errorf("Refres binding %s failed", b.id)
				continue
			}
		}
	}()
}

func (b *ServicesBinding) unbind() {
	b.stopping = true
}
