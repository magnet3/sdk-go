package mongo

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"gitlab.com/magnet3/sdk-go/discovery"
	"gopkg.in/mgo.v2"
	"sync"
	"time"
)

const Service = "mongo"
const PrimaryTag = "primary"
const KVPath = "magnet3/params/mongo/db"

const MagnetDb = "magnet"
const MediaCollection = "media"
const CountriesCollection = "countries"
const GenresCollection = "genres"
const SpacesCollection = "spaces"

type Binding struct {
	consul        *discovery.Consul
	serviceName   string
	defaultDBName string
	Session       *mgo.Session
	databases
	collections
}

type databases struct {
	sync.RWMutex
	items map[string]*mgo.Database
}

type collections struct {
	sync.RWMutex
	items map[string]*mgo.Collection
}

func Bind(c *discovery.Consul, service string) (*Binding, error) {
	b := &Binding{
		consul:      c,
		serviceName: service,
	}
	err := b.connect()
	if err != nil {
		return nil, err
	}
	b.watch()
	return b, nil
}

func (m *Binding) watch() {
	t := time.NewTicker(5 * time.Second)
	go func() {
		for range t.C {
			err := m.Session.Ping()
			if err != nil {
				log.Warn("Mongo ping failed, try reconnect")
				err = m.connect()
				if err == nil {
					log.Warn("Mongo reconnect success")
				} else {
					log.Errorf("Mongo reconnect failed: %s", err.Error())
				}
			}
		}
	}()
}

func (m *Binding) connect() error {
	b, err := m.consul.NewServiceWitTagBinding(m.serviceName, PrimaryTag, discovery.DefaultRefreshTimeout)
	if err != nil {
		return err
	}
	s := b.GetRoundRobinService()
	u := fmt.Sprintf("mongodb://%s:%d", s.ServiceAddress, s.ServicePort)
	sess, err := mgo.Dial(u)
	if err != nil {
		return err
	}
	log.Infof("Mongo connect to %s", u)
	db := m.consul.GetStringParam(KVPath, MagnetDb)
	m.Session = sess
	m.defaultDBName = db
	m.databases = databases{
		items: map[string]*mgo.Database{},
	}
	m.collections = collections{
		items: map[string]*mgo.Collection{},
	}
	return nil
}

func (m *Binding) Database(dbName string) *mgo.Database {
	m.databases.RLock()
	db, ok := m.databases.items[dbName]
	m.databases.RUnlock()
	if ok {
		return db
	}
	db = m.Session.DB(dbName)
	m.databases.Lock()
	m.databases.items[dbName] = db
	m.databases.Unlock()
	return db
}

func (m *Binding) DefaultDatabase() *mgo.Database {
	return m.Database(m.defaultDBName)
}

func (m *Binding) Collection(dbName, colName string) *mgo.Collection {
	k := fmt.Sprintf("%s.%s", dbName, colName)
	m.collections.RLock()
	col, ok := m.collections.items[k]
	m.collections.RUnlock()
	if ok {
		return col
	}
	col = m.Database(dbName).C(colName)
	m.collections.Lock()
	m.collections.items[colName] = col
	m.collections.Unlock()
	return col
}

func (m *Binding) CollectionDefaultDatabase(colName string) *mgo.Collection {
	return m.Collection(m.defaultDBName, colName)
}
