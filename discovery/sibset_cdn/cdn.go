package sibset_cdn

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type CDN struct {
	host    string
	port    int
	drive   string
	project string
	timeout time.Duration
}

func NewCDN(host string, port int, drive string, project string) *CDN {
	return &CDN{
		host:    host,
		port:    port,
		drive:   drive,
		project: project,
		timeout: 1 * time.Second,
	}
}

func (self *CDN) buildUrls(fileName string) (string, string) {
	d := time.Now().Format("2006/01/02")
	in := fmt.Sprintf("http://%s:%d/%s/%s/%s/%s", self.host, self.port, self.drive, self.project, d, fileName)
	out := fmt.Sprintf("http://%s:%d/%s/%s/%s", self.host, self.port, self.project, d, fileName)
	return in, out
}

func (self *CDN) UploadContent(content []byte, fileName string) (string, error) {
	in, out := self.buildUrls(fileName)
	u, err := url.Parse(in)
	if err != nil {
		return "", err
	}
	reader := bytes.NewReader(content)
	closer := ioutil.NopCloser(reader)
	req := &http.Request{
		URL:           u,
		Method:        http.MethodPut,
		Body:          closer,
		ContentLength: int64(reader.Len()),
	}
	client := &http.Client{
		Timeout: self.timeout,
	}
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	if res.StatusCode == 204 || res.StatusCode == 201 {
		return out, nil
	}
	return "", errors.New(fmt.Sprintf("Incorrect server response status: %s", res.Status))
}
