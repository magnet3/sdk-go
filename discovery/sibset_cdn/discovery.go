package sibset_cdn

import (
	"fmt"
	"gitlab.com/magnet3/sdk-go/discovery"
	"time"
)

const DefaultCdnHost = "cdn.211.ru"
const DefaultCdnDrive = "d2"
const DefaultCdnProject = "magnet.sibset.ru"

func keyPath(key string) string {
	return fmt.Sprintf("magnet3/params/cdn/%s", key)
}

func GetCdnHost(c *discovery.Consul, d string) string {
	return c.GetStringParam(keyPath("host"), d)
}

func GetCdnPort(c *discovery.Consul, d int) int {
	return c.GetIntParam(keyPath("port"), d)
}

func GetCdnDrive(c *discovery.Consul, d string) string {
	return c.GetStringParam(keyPath("drive"), d)
}

func GetCdnProject(c *discovery.Consul, d string) string {
	return c.GetStringParam(keyPath("project"), d)
}


type Binding struct {
	consul         *discovery.Consul
	cdn            *CDN
	defaultHost    string
	defaultPort    int
	defaultDrive   string
	defaultProject string
}

func BindDefault(c *discovery.Consul) (*Binding, error) {
	return Bind(c, DefaultCdnHost, 80, DefaultCdnDrive, DefaultCdnProject)
}

func Bind(c *discovery.Consul, host string, port int, drive, project string) (*Binding, error) {
	b := &Binding{
		consul:         c,
		defaultHost:    host,
		defaultPort:    port,
		defaultDrive:   drive,
		defaultProject: project,
	}
	b.discover()
	b.rotate()
	return b, nil
}

func (b *Binding) rotate() {
	t := time.NewTicker(5 * time.Second)
	go func() {
		for range t.C {
			b.discover()
		}
	}()
}

func (b *Binding) discover() {
	b.cdn = NewCDN(
		GetCdnHost(b.consul, b.defaultHost),
		GetCdnPort(b.consul, b.defaultPort),
		GetCdnDrive(b.consul, b.defaultDrive),
		GetCdnProject(b.consul, b.defaultProject),
	)
}

func (b *Binding) GetCDN() *CDN {
	return b.cdn
}
