package mysql

import (
	"database/sql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/magnet3/sdk-go/discovery"
	"time"
)

const Service = "mysql"

const MagnetDbName = "magnet"
const MagnetUsername = "magnet"
const MagnetPassword = "magnet"
const MagnetCharset = "utf8"
const MagnetCollation = "utf8_general_ci"

func keyPath(key string) string {
	return fmt.Sprintf("magnet3/params/mysql/%s", key)
}

type Binding struct {
	consul      *discovery.Consul
	serviceName string
	database    *sql.DB
}

func Bind(c *discovery.Consul, service string) (*Binding, error) {
	b := &Binding{
		consul:      c,
		serviceName: service,
	}
	err := b.connect()
	if err != nil {
		return nil, err
	}
	b.watch()
	return b, nil
}

func (m *Binding) watch() {
	t := time.NewTicker(5 * time.Second)
	go func() {
		for range t.C {
			err := m.database.Ping()
			if err != nil {
				log.Warn("MySQL ping failed, try reconnect")
				err = m.connect()
				if err == nil {
					log.Warn("MySQL reconnect success")
				} else {
					log.Errorf("MySQL reconnect failed: %s", err.Error())
				}
			}
		}
	}()
}

func (m *Binding) connect() error {
	dsn, err := DSNFromConsul(m.consul, MagnetDbName)
	if err != nil {
		return err
	}
	db, err := sql.Open("mysql", dsn.String())
	if err != nil {
		return err
	}
	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(50)
	log.Infof("MySQL connect to %s", dsn.String())
	m.database = db
	return nil
}

func (m *Binding) DefaultDatabase() *sql.DB {
	return m.database
}
