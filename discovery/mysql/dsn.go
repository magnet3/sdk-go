package mysql

import (
	"fmt"
	"gitlab.com/magnet3/sdk-go/discovery"
)

type DataSourceName struct {
	Host      string
	Port      int
	Username  string
	Password  string
	DbName    string
	Charset   string
	Collation string
}

func DSN(host string, port int, dbname, username, password string) DataSourceName {
	return DataSourceName{
		Host:      host,
		Port:      port,
		DbName:    dbname,
		Username:  username,
		Password:  password,
		Charset:   MagnetCharset,
		Collation: MagnetCollation,
	}
}

func DSNFromConsul(c *discovery.Consul, dbname string) (dsn DataSourceName, err error) {
	s, err := c.DiscoverFirstService(Service)
	if err != nil {
		return
	}
	dsn = DataSourceName{
		Username: c.GetStringParam(keyPath("username"), MagnetUsername),
		// @todo Refactoring with Vault
		Password:  c.GetStringParam(keyPath("password"), MagnetPassword),
		Host:      s.ServiceAddress,
		Port:      s.ServicePort,
		DbName:    c.GetStringParam(keyPath("dbname"), dbname),
		Charset:   c.GetStringParam(keyPath("charset"), MagnetCharset),
		Collation: c.GetStringParam(keyPath("collation"), MagnetCollation),
	}
	return
}

func (dsn DataSourceName) String() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=%s&collation=%s",
		dsn.Username,
		dsn.Password,
		dsn.Host,
		dsn.Port,
		dsn.DbName,
		dsn.Charset,
		dsn.Collation)
}
