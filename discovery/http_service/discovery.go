package http_service

import (
	"fmt"
	"gitlab.com/magnet3/sdk-go/discovery"
	"net/http"
	"net/url"
)

type Binding struct {
	consul         *discovery.Consul
	serviceBinding *discovery.ServicesBinding
	userAgent      string
}

func Bind(c *discovery.Consul, thisNode, service, tag string) (*Binding, error) {
	binding, err := c.NewServiceWitTagBinding(service, tag, discovery.DefaultRefreshTimeout)
	if err != nil {
		return nil, err
	}
	b := &Binding{
		consul:         c,
		serviceBinding: binding,
		userAgent:      fmt.Sprintf("magnet3/%s-node", thisNode),
	}
	return b, nil
}

func (b *Binding) Request(path, rid string) *http.Request {
	service := b.serviceBinding.GetRoundRobinService()
	host := fmt.Sprintf("%s:%d", service.ServiceAddress, service.ServicePort)
	uri := url.URL{
		Scheme: "http",
		Host:   host,
		Path:   path,
	}
	header := http.Header{}
	header.Add("User-Agent", b.userAgent)
	header.Add("X-Request-Id", rid)
	return &http.Request{
		URL:    &uri,
		Header: header,
	}
}
