package sphinx

import (
	"bytes"
	"encoding/gob"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/slvmnd/gosphinx"
	"gitlab.com/magnet3/sdk-go/discovery"
	"time"
)

const Service = "sphinx"

const MagnetSpace = "magnet"
const KaraokeSpace = "karaoke"

var Timeout = 10 * time.Second

func keyPath(key string) string {
	return fmt.Sprintf("magnet3/params/sphinx/%s", key)
}

type Binding struct {
	consul      *discovery.Consul
	serviceName string
	sphinx      *gosphinx.SphinxClient
}

func Bind(c *discovery.Consul, service string) (*Binding, error) {
	b := &Binding{
		consul:      c,
		serviceName: service,
	}
	err := b.connect()
	if err != nil {
		return nil, err
	}
	b.watch()
	return b, nil
}

func (m *Binding) watch() {
	t := time.NewTicker(5 * time.Second)
	go func() {
		for range t.C {
			if m.sphinx.IsConnectError() {
				log.Warn("Sphinx connection failed, try reconnect")
				err := m.connect()
				if err == nil {
					log.Warn("Sphinx reconnect success")
				} else {
					log.Errorf("Sphinx reconnect failed: %s", err.Error())
				}
			}
		}
	}()
}

func (m *Binding) connect() error {
	b, err := m.consul.NewServiceBinding(m.serviceName)
	if err != nil {
		return err
	}
	s := b.GetRoundRobinService()
	p := gosphinx.NewSphinxClient()
	p.SetMatchMode(gosphinx.SPH_MATCH_EXTENDED2)
	p.SetRankingMode(gosphinx.SPH_RANK_SPH04)
	p.SetLimits(0, 9999, 9999, 0)
	t := m.consul.GetDurationParam(keyPath("timeout"), time.Millisecond, 500*time.Millisecond)
	p.SetConnectTimeout(int(t) / int(time.Millisecond))
	err = p.SetServer(s.ServiceAddress, s.ServicePort)
	if err != nil {
		return err
	}
	log.Infof("Sphinx connect to %s:%d", s.ServiceAddress, s.ServicePort)
	m.sphinx = p
	return nil
}

type Match struct {
	Alias  string `json:"alias"`
	Weight int    `json:"weight"`
}

func (m *Binding) Query(query, space string) (r []Match, err error) {
	searched, err := m.sphinx.Query(query, space, "")
	if err != nil {
		return
	}
	for _, m := range searched.Matches {
		arr, _ := decode(m.AttrValues[1])
		v := string(arr[4:])
		r = append(r, Match{
			Alias:  v,
			Weight: m.Weight,
		})
	}
	return r, nil
}

func decode(key interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(key)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
