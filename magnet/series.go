package magnet

type Season struct {
	Number      int       `bson:"number"       json:"number"`
	Alias       string    `bson:"alias"        json:"alias"`
	Name        string    `bson:"name"         json:"name,omitempty"`
	Origin      string    `bson:"origin"       json:"origin,omitempty"`
	Description string    `bson:"description"  json:"description,omitempty"`
	Episodes    []Episode `bson:"episodes"     json:"episodes,omitempty"`
}

func EmptySeason() Season {
	return Season{}
}

func (s Season) IsEmpty() bool {
	return s.Alias == ""
}

func (s *Season) GetEpisode(number int) Episode {
	for _, episode := range s.Episodes {
		if episode.Number == number {
			return episode
		}
	}
	return EmptyEpisode()
}

func (s *Season) SetEpisode(episode Episode) {
	for i, source := range s.Episodes {
		if source.Number == episode.Number {
			s.Episodes[i] = episode
			return
		}
	}
	s.Episodes = append(s.Episodes, episode)
}

func (s *Season) RemoveEpisode(episode Episode) {
	result := []Episode{}
	for _, source := range s.Episodes {
		if source.Number != episode.Number {
			result = append(result, source)
		}
	}
	s.Episodes = result
}


type Episode struct {
	Number      int    `bson:"number"       json:"number"`
	Alias       string `bson:"alias"        json:"alias"`
	Name        string `bson:"name"         json:"name,omitempty"`
	Origin      string `bson:"origin"       json:"origin,omitempty"`
	Description string `bson:"description"  json:"description,omitempty"`
	Links       []Link `bson:"links"        json:"links,omitempty"`
}

func EmptyEpisode() Episode {
	return Episode{}
}

func (e Episode) IsEmpty() bool {
	return e.Alias == ""
}
