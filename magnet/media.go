package magnet

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type MediaGenre struct {
	Alias string `bson:"alias"  json:"alias"  binding:"required"`
	Name  string `bson:"name"   json:"name"   binding:"required"`
}

type MediaCountry struct {
	Alias string `bson:"alias"  json:"alias"  binding:"required"`
	Name  string `bson:"name"   json:"name"   binding:"required"`
}

type Media struct {
	Id           bson.ObjectId  `bson:"_id"                    json:"-"`
	Alias        string         `bson:"alias"                  json:"alias"                    binding:"required"`
	Name         string         `bson:"name"                   json:"name"                     binding:"required"`
	Origin       string         `bson:"origin,omitempty"       json:"origin,omitempty"`
	Description  string         `bson:"description,omitempty"  json:"description,omitempty"`
	Type         string         `bson:"type"                   json:"type"                     binding:"required"`
	Released     time.Time      `bson:"released,omitempty"     json:"released,omitempty"`
	Started      time.Time      `bson:"started,omitempty"      json:"started,omitempty"`
	Finished     time.Time      `bson:"finished,omitempty"     json:"finished,omitempty"`
	Added        time.Time      `bson:"added,omitempty"        json:"added"`
	Updated      time.Time      `bson:"updated,omitempty"      json:"updated,omitempty"`
	Images       []Image        `bson:"images,omitempty"       json:"images,omitempty"`
	Trailers     []Link         `bson:"trailers,omitempty"     json:"trailers,omitempty"`
	Restrictions *Restrictions  `bson:"restrictions,omitempty" json:"restrictions,omitempty"`
	Genres       []MediaGenre   `bson:"genres,omitempty"       json:"genres,omitempty"`
	Countries    []MediaCountry `bson:"countries,omitempty"    json:"countries,omitempty"`
	Links        []Link         `bson:"links,omitempty"        json:"links,omitempty"`
	Seasons      []Season       `bson:"seasons,omitempty"      json:"seasons,omitempty"`
	Persons      *Persons       `bson:"persons,omitempty"      json:"persons,omitempty"`
	Likes        []Like         `bson:"likes,omitempty"        json:"likes,omitempty"`
	Rating       *Rating        `bson:"ratings,omitempty"      json:"rating,omitempty"`
	Spaces       []string       `bson:"spaces"                 json:"spaces,omitempty"         binding:"required"`
	Visible      bool           `bson:"visible"                json:"visible,omitempty"`
	External     *External      `bson:"external,omitempty"     json:"external,omitempty"`
	Tags         []string       `bson:"tags,omitempty"         json:"tags,omitempty"`
	Year         int            `bson:"year,omitempty"         json:"-"`
}

func EmptyMedia() Media {
	now := time.Now()
	return Media{
		Id:      bson.NewObjectId(),
		Updated: now,
		Added:   now,
	}
}

func (m Media) IsEmpty() bool {
	return m.Alias == "" || m.Name == ""
}

func (m *Media) GetSeason(number int) Season {
	for _, season := range m.Seasons {
		if season.Number == number {
			return season
		}
	}
	return EmptySeason()
}

func (m *Media) SetSeason(season Season) {
	for i, source := range m.Seasons {
		if source.Number == season.Number {
			m.Seasons[i] = season
			return
		}
	}
	m.Seasons = append(m.Seasons, season)
}

func (m *Media) RemoveSeason(season Season) {
	result := []Season{}
	for _, source := range m.Seasons {
		if source.Number != season.Number {
			result = append(result, source)
		}
	}
	m.Seasons = result
}
