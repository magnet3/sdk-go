package magnet

const KaraokeSpace = "karaoke"
const MagnetSpace = "magnet"

var Spaces = []string{KaraokeSpace, MagnetSpace}

const TrackType = "track"
const MovieType = "movie"
const SeriesType = "series"

var Types = []string{TrackType, MovieType, SeriesType}

const Rars0Plus = "0+"
const Rars6Plus = "6+"
const Rars12Plus = "12+"
const Rars16Plus = "16+"
const Rars18Plus = "18+"

var RarsRatings = []string{Rars0Plus, Rars6Plus, Rars12Plus, Rars16Plus, Rars18Plus}
