package magnet

import "gopkg.in/mgo.v2/bson"

// ============================ Genres ============================

type Genre struct {
	Id      bson.ObjectId `bson:"_id"      json:"-"`
	Alias   string        `bson:"alias"    json:"alias"   binding:"required"`
	Name    string        `bson:"name"     json:"name"    binding:"required"`
	Spaces  []string      `bson:"spaces"   json:"spaces"  binding:"required"`
	Visible bool          `bson:"visible"  json:"visible"`
}

func EmptyGenre() Genre {
	return Genre{
		Id: bson.NewObjectId(),
	}
}

func (c Genre) IsEmpty() bool {
	return c.Alias == "" || c.Name == ""
}

// ========================== Countries ===========================

type Country struct {
	Id    bson.ObjectId `bson:"_id"    json:"-"`
	Alias string        `bson:"alias"  json:"alias"  binding:"required"`
	Name  string        `bson:"name"   json:"name"   binding:"required"`
}

func (c Country) IsEmpty() bool {
	return c.Alias == "" || c.Name == ""
}

func EmptyCountry() Country {
	return Country{
		Id: bson.NewObjectId(),
	}
}
