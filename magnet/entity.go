package magnet

import "gopkg.in/mgo.v2/bson"

type Space struct {
	Id    bson.ObjectId `bson:"_id"    json:"-"`
	Alias string        `bson:"alias"  json:"alias"`
	Name  string        `bson:"name"   json:"name"`
}

type Link struct {
	Name string   `bson:"name"            json:"name"            binding:"required"`
	Link string   `bson:"link"            json:"link"            binding:"required"`
	File string   `bson:"file"            json:"file"            binding:"required"`
	Type string   `bson:"type"            json:"type"            binding:"required"`
	Size int64    `bson:"size"            json:"size"            binding:"required"`
	Tags []string `bson:"tags,omitempty"  json:"tags,omitempty"`
}

type Image struct {
	Url  string   `bson:"url"  json:"url"   binding:"required"`
	Tags []string `bson:"tags" json:"tags"  binding:"required"`
}

type External struct {
	Kinopoisk int    `bson:"kinopoisk,omitempty" json:"kinopoisk,omitempty"`
	Imdb      string `bson:"imdb,omitempty"      json:"imdb,omitempty"`
}

type Persons struct {
	Actors    []Person `bson:"actors"    json:"actors,omitempty"`
	Directors []Person `bson:"directors" json:"directors,omitempty"`
	Producers []Person `bson:"producers" json:"producers,omitempty"`
	Writers   []Person `bson:"writers"   json:"writers,omitempty"`
	Operators []Person `bson:"operators" json:"operators,omitempty"`
	Editors   []Person `bson:"editors"   json:"editors,omitempty"`
	Composers []Person `bson:"composers" json:"composers,omitempty"`
}

type Person struct {
	Name  string `bson:"name"  json:"name"`
	Alias string `bson:"alias" json:"alias"`
}

type Like struct {
	Name   string `bson:"name"             json:"name"`
	Origin string `bson:"origin,omitempty" json:"origin,omitempty"`
	Alias  string `bson:"alias"            json:"alias"`
}

type Rating struct {
	Kinopoisk float64 `bson:"kp,omitempty" json:"kinopoisk,omitempty"`
}

type Restrictions struct {
	Mpaa string `bson:"mpaa,omitempty" json:"mpaa,omitempty"`
	Rars string `bson:"rars,omitempty" json:"rars,omitempty"`
}
