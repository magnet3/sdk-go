package api

import "github.com/ant0ine/go-json-rest/rest"

type RoutingController interface {
	Routes() []*rest.Route
}

func MakeControllerRouter(c ...RoutingController) (rest.App, error) {
	r := make([]*rest.Route, 0)
	for _, v := range c {
		r = append(r, v.Routes()...)
	}
	return rest.MakeRouter(r...)
}
