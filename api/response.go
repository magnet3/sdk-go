package api

import (
	"fmt"
	"github.com/ant0ine/go-json-rest/rest"
	"net/http"
	"reflect"
)

func Continue(w rest.ResponseWriter) {
	w.(http.ResponseWriter).WriteHeader(http.StatusContinue)
}

func Created(w rest.ResponseWriter, location string) {
	w.Header().Add("Location", location)
	w.(http.ResponseWriter).WriteHeader(http.StatusCreated)
	w.WriteJson(map[string]interface{}{
		"code":    http.StatusCreated,
		"message": "OK",
		"result":  location,
	})
}

func CreatedResult(w rest.ResponseWriter, v interface{}, location string) {
	w.Header().Add("Location", location)
	w.(http.ResponseWriter).WriteHeader(http.StatusCreated)
	w.WriteJson(map[string]interface{}{
		"code":    http.StatusCreated,
		"message": "OK",
		"result":  v,
	})
}

func Accepted(w rest.ResponseWriter) {
	w.(http.ResponseWriter).WriteHeader(http.StatusAccepted)
}

func NoContent(w rest.ResponseWriter) {
	w.(http.ResponseWriter).WriteHeader(http.StatusNoContent)
}

func SuccessResult(w rest.ResponseWriter, result interface{}) {
	w.WriteHeader(http.StatusOK)
	w.WriteJson(map[string]interface{}{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  result,
	})
}

func SuccessItems(w rest.ResponseWriter, total int, items interface{}) {
	switch reflect.TypeOf(items).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(items)
		if s.Len() == 0 {
			items = make([]int, 0)
		}
	}

	w.WriteHeader(http.StatusOK)
	w.WriteJson(map[string]interface{}{
		"code":    http.StatusOK,
		"message": "OK",
		"result": map[string]interface{}{
			"total": total,
			"items": items,
		},
	})
}

func SuccessFile(w rest.ResponseWriter, b []byte, f, ct string) {
	w.Header().Add("Content-Disposition", fmt.Sprintf("inline; filename=\"%s\"", f))
	w.Header().Add("Content-Type", ct)
	w.(http.ResponseWriter).Write(b)
}

func SuccessEmpty(w rest.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	w.WriteJson(map[string]interface{}{
		"code":    http.StatusOK,
		"message": "OK",
	})
}

func failure(w rest.ResponseWriter, code int, message string) {
	w.WriteHeader(code)
	w.WriteJson(map[string]interface{}{
		"code":    code,
		"message": message,
	})
}

func BadRequestf(w rest.ResponseWriter, format string, a ...string) {
	BadRequest(w, fmt.Sprintf(format, a))
}

func BadRequest(w rest.ResponseWriter, message string) {
	failure(w, http.StatusBadRequest, message)
}

func Forbiddenf(w rest.ResponseWriter, format string, a ...string) {
	Forbidden(w, fmt.Sprintf(format, a))
}

func Forbidden(w rest.ResponseWriter, message string) {
	failure(w, http.StatusForbidden, message)
}

func NotFoundf(w rest.ResponseWriter, format string, a ...string) {
	NotFound(w, fmt.Sprintf(format, a))
}

func NotFound(w rest.ResponseWriter, message string) {
	failure(w, http.StatusNotFound, message)
}

func NotAcceptablef(w rest.ResponseWriter, format string, a ...string) {
	NotAcceptable(w, fmt.Sprintf(format, a))
}

func NotAcceptable(w rest.ResponseWriter, message string) {
	failure(w, http.StatusNotAcceptable, message)
}

func NotFoundEmpty(w rest.ResponseWriter) {
	w.(http.ResponseWriter).WriteHeader(http.StatusNotFound)
}

func MethodNotAllowed(w rest.ResponseWriter, message string) {
	failure(w, http.StatusMethodNotAllowed, message)
}

func Conflictf(w rest.ResponseWriter, format string, a ...string) {
	Conflict(w, fmt.Sprintf(format, a))
}

func Conflict(w rest.ResponseWriter, message string) {
	failure(w, http.StatusConflict, message)
}

func FailedDependency(w rest.ResponseWriter, message string) {
	failure(w, http.StatusFailedDependency, message)
}

func InternalErrorf(w rest.ResponseWriter, format string, a ...string) {
	InternalError(w, fmt.Sprintf(format, a))
}

func InternalError(w rest.ResponseWriter, message string) {
	failure(w, http.StatusInternalServerError, message)
}
