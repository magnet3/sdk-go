package api

import (
	"errors"
	"fmt"
	"github.com/ant0ine/go-json-rest/rest"
	"io/ioutil"
	"strconv"
	"strings"
)

func PaginationDefault(req *rest.Request, o, l int) (offset int, limit int, err error) {
	if offset, err = QueryInt(req, "offset", o); err != nil {
		err = errors.New("Incorrect pagination offset value")
		return
	}
	if limit, err = QueryInt(req, "limit", l); err != nil {
		err = errors.New("Incorrect pagination limit value")
		return
	}
	return
}

func QueryString(req *rest.Request, q string, d string) string {
	if v := req.Request.URL.Query().Get(q); v != "" {
		return v
	}
	return d

}

func QueryInt(req *rest.Request, q string, d int) (i int, err error) {
	if v := req.Request.URL.Query().Get(q); v != "" {
		i, err = strconv.Atoi(v)
		if err != nil {
			err = fmt.Errorf("Incorrect int value in %s", q)
		}
		return
	} else {
		i = d
	}
	return
}

func QueryFloat(req *rest.Request, q string, d float64) (f float64, err error) {
	if v := req.Request.URL.Query().Get(q); v != "" {
		f, err = strconv.ParseFloat(v, 10)
		if err != nil {
			err = fmt.Errorf("Incorrect float value in %s", q)
		}
		return
	} else {
		f = d
	}
	return
}

func QueryBool(req *rest.Request, q string, d bool) bool {
	if v := req.Request.URL.Query().Get(q); v != "" {
		return v == "1"
	}
	return d
}

func QueryStringSlice(req *rest.Request, q string, d []string) []string {
	if v := req.Request.URL.Query().Get(q); v != "" {
		return strings.Split(v, ",")
	}
	return d
}

func QueryIntSlice(req *rest.Request, q string, d []int) (r []int, err error) {
	if v := req.Request.URL.Query().Get(q); v != "" {
		for _, s := range strings.Split(v, ",") {
			i, convErr := strconv.Atoi(s)
			if convErr != nil {
				err = convErr
				return
			}
			r = append(r, i)
		}
		return
	}
	r = d
	return

}

func FormFile(req *rest.Request, n string) (b []byte, err error) {
	f, _, err := req.FormFile(n)
	if err != nil {
		return
	}
	b, err = ioutil.ReadAll(f)
	defer f.Close()
	if err != nil {
		return
	}
	if len(b) == 0 {
		err = errors.New("File is empty")
		return
	}
	return
}

func AcceptLanguage(req *rest.Request, d string) string {
	h := req.Header.Get("Accept-Language")
	if h == "" {
		h = d
	}
	return h
}
