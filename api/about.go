package api

import (
	"encoding/json"
	"github.com/ant0ine/go-json-rest/rest"
	"io/ioutil"
)

type About struct {
	Name        string `json:"name"`
	VcsHash     string `json:"vcs_hash"`
	VcsBranch   string `json:"vcs_branch"`
	DockerImage string `json:"docker_image"`
}

type AboutResource struct {
	About
}

func AboutController(f string) *AboutResource {
	c, err := ioutil.ReadFile(f)
	if err != nil {
		return &AboutResource{}
	}
	a := About{}
	err = json.Unmarshal(c, &a)
	return &AboutResource{
		About: a,
	}
}

func (a *AboutResource) Routes() []*rest.Route {
	r := make([]*rest.Route, 0)

	r = append(r, rest.Get("/about", a.handleAboutGet))

	return r
}

func (a *AboutResource) handleAboutGet(w rest.ResponseWriter, req *rest.Request) {
	SuccessResult(w, a.About)
}
