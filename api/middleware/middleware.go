package middleware

import (
	"github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
	"log"
	"os"
)

const PoweredBy = "Magnet3"

var Node, _ = os.Hostname()

var requestIdMiddleware = &XRequestIdMiddleware{}

var logMiddleware = &LogMiddleware{}
var logger = log.New(logrus.StandardLogger().Writer(), "", 0)

func ProdStackDefault() []rest.Middleware {
	return ProdStack(PoweredBy, Node)
}

func ProdStack(poweredBy, hostname string) []rest.Middleware {
	return []rest.Middleware{
		logMiddleware,
		&rest.TimerMiddleware{},
		&rest.RecorderMiddleware{},
		&rest.PoweredByMiddleware{
			XPoweredBy: poweredBy,
		},
		&rest.RecoverMiddleware{},
		requestIdMiddleware,
		&XNodeMiddleware{
			Node: hostname,
		},
	}
}

func DevStack(poweredBy, hostname string) []rest.Middleware {
	return []rest.Middleware{
		&rest.AccessLogApacheMiddleware{
			Logger: logger,
			Format: rest.CommonLogFormat,
		}, &rest.TimerMiddleware{},
		&rest.RecorderMiddleware{},
		&rest.PoweredByMiddleware{
			XPoweredBy: poweredBy,
		},
		&rest.RecoverMiddleware{
			EnableResponseStackTrace: true,
		},
		&rest.JsonIndentMiddleware{},
		requestIdMiddleware,
		&XNodeMiddleware{
			Node: hostname,
		},
	}
}

func DevStackkDefault() []rest.Middleware {
	return DevStack(PoweredBy, Node)
}
