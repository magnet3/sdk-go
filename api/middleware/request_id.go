package middleware

import (
	"github.com/ant0ine/go-json-rest/rest"
	"github.com/satori/go.uuid"
	"crypto/md5"
	"fmt"
)

const xRequestIdName = "X-Request-Id"

type XRequestIdMiddleware struct{}

func (mw *XRequestIdMiddleware) MiddlewareFunc(h rest.HandlerFunc) rest.HandlerFunc {
	return func(w rest.ResponseWriter, r *rest.Request) {
		rid := r.Header.Get(xRequestIdName)
		if rid == "" {
			rid = fmt.Sprintf("%x", md5.Sum(uuid.NewV4().Bytes()))
		}
		w.Header().Add(xRequestIdName, rid)
		r.Env["REQUEST_ID"] = rid
		h(w, r)
	}
}
