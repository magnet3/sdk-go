package middleware

import (
	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
	"time"
)

type LogMiddleware struct{}

func (mw *LogMiddleware) MiddlewareFunc(h rest.HandlerFunc) rest.HandlerFunc {
	return func(w rest.ResponseWriter, r *rest.Request) {
		// call the handler
		h(w, r)
		l := makeAccessLogJsonRecord(r)
		log.WithFields(l.Fields()).Infof("%s %s %s", l.RequestMethod, l.RequestURI, l.RequestProto)
	}
}

type AccessLogRecord struct {
	RemoteAddr    string
	BytesSent     int64
	RequestMethod string
	ElapsedTime   *time.Duration
	RequestId     string
	RequestURI    string
	RequestProto  string
	StatusCode    int
	UserAgent     string
	Referer       string
	Host          string
}

func makeAccessLogJsonRecord(r *rest.Request) *AccessLogRecord {
	var statusCode int
	if r.Env["STATUS_CODE"] != nil {
		statusCode = r.Env["STATUS_CODE"].(int)
	}

	var elapsedTime *time.Duration
	if r.Env["ELAPSED_TIME"] != nil {
		elapsedTime = r.Env["ELAPSED_TIME"].(*time.Duration)
	}

	var requestId string
	if r.Env["REQUEST_ID"] != nil {
		requestId = r.Env["REQUEST_ID"].(string)
	}

	var bytesWriten int64
	if r.Env["BYTES_WRITTEN"] != nil {
		bytesWriten = r.Env["BYTES_WRITTEN"].(int64)
	}

	referer := r.Header.Get("Referer")

	return &AccessLogRecord{
		RemoteAddr:    r.RemoteAddr,
		BytesSent:     bytesWriten,
		RequestMethod: r.Method,
		ElapsedTime:   elapsedTime,
		RequestId:     requestId,
		RequestURI:    r.RequestURI,
		RequestProto:  r.Proto,
		StatusCode:    statusCode,
		UserAgent:     r.UserAgent(),
		Referer:       referer,
		Host:          r.Host,
	}
}

func (r *AccessLogRecord) Fields() log.Fields {
	f := log.Fields{
		"remote_addr":     r.RemoteAddr,
		"bytes_sent":      r.BytesSent,
		"request_method":  r.RequestMethod,
		"request_id":      r.RequestId,
		"status":          r.StatusCode,
		"http_user_agent": r.UserAgent,
		"http_referer":    r.Referer,
		"http_host":       r.Host,
	}
	if r.ElapsedTime != nil {
		f["request_time"] = *r.ElapsedTime / time.Millisecond
	}
	return f
}
