package middleware

import (
	"github.com/ant0ine/go-json-rest/rest"
)

const xNodeName = "X-Node"

type XNodeMiddleware struct{
	Node string
}

func (mw *XNodeMiddleware) MiddlewareFunc(h rest.HandlerFunc) rest.HandlerFunc {
	return func(w rest.ResponseWriter, r *rest.Request) {
		w.Header().Add(xNodeName, mw.Node)
		h(w, r)
	}
}
