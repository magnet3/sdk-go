package log

import (
	log "github.com/Sirupsen/logrus"
	"os"
)

func StdoutLogging(debug bool) {
	log.SetOutput(os.Stdout)
	if debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}
	log.SetFormatter(&JSONFormatter{})
}
